# Subtitles to VIA converter

This Python script creates VIA Projects files for the data annotation task of the [CONTENT4ALL Project](http://content4all-project.eu/).

The aim of the data annotation task is to obtain ground-truth data in order to translate spoken/written language to Sign language in a form that can be passed to an animation system.

In order to obtain ground-truth data, videos with Sign Language contents shall be manually aligned with written content.
To ease the task, the transcription is extracted from the relative subtitle file (SRT file) and then split in Sentences according to the following rules:

* period (`.`)
* semicolon (`;`)
* exclamation mark (`!`)
* question mark (`?`)
* ellipsis (`…`)

The resulting Sentences are aligned to the video (according to the timestamps of the SRT file) and then packed inside a VIA Project file, that the Annotator can edit with [CONTENT4ALL based on VIA](https://gitlab.com/content4all-public/data-annotation/content4all-based-on-via) tool.

## Getting Started

### Prerequisites

* Python 3.7 (or greater)
* Python SRT 3.3.0 library ( `pip install srt` )

### Usage

1. Prepare a CSV file with the following header:
    ```
    project_name,video_url,srt_file_path,srt_file_encoding
    ```
2. Add one row to for each VIA Project that shall be created.  
    Each row shall contain:
    * the project name
    * the MP4/H.264 video URL
    * the path to the SRT file related to the video
    * the encoding of the SRT file (see [Standard Encodings](https://docs.python.org/3/library/codecs.html#standard-encodings))
   > E.G.
   > ```
   > project_name,video_url,srt_file_path,srt_file_encoding
   > myProyect0,https://mysite.com/video0.mp4,path/to/sub0.srt,utf_8
   > myProyect1,https://mysite.com/video1.mp4,path/to/sub1.srt,utf_8
   > ...
   > ```
3. Launch `srt2via.py` with the following parameters:
    * CSV file path 
    * VIA Projects output folder
    > E.G.  
    > `srt2via.py -i path/to/file.csv -o path/to/output/folder`
4. The script creates one VIA Project file for each row of the CSV file

### Development

This script was developed with Visual Studio Code.  
The `.vscode/launch.json` and `.vscode/settings.json` are provided for convenience.

## Authors

This script was implemented by Marco Giovanelli for FINCONS GROUP AG within the CONTENT4ALL Project.  
For any further information, please send an email to [content4all-dev@finconsgroup.com](mailto:content4all-dev@finconsgroup.com).

## License

This script is released as Open Source according to the "2-Clause BSD License":

```
Copyright (c) 2020, developed by Marco Giovanelli for FINCONS GROUP AG
within the CONTENT4ALL Project.
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
```
